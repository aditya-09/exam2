package org.example;

public class No2 {
    /** Print the calendar for a month in a year */

    static void month(String month, Integer year){
        switch (month){
            case "Januari" :
                printMonth(year, 1);
                break;
            case "Februari" :
                printMonth(year, 2);
                break;
            case "Maret" :
                printMonth(year, 3);
                break;
            case "April" :
                printMonth(year, 4);
                break;
            case "Mei" :
                printMonth(year, 5);
                break;
            case "Juni" :
                printMonth(year, 6);
                break;
            case "Juli" :
                printMonth(year, 7);
                break;
            case "Agustus" :
                printMonth(year, 8);
                break;
            case "September" :
                printMonth(year, 9);
                break;
            case "Oktober" :
                printMonth(year, 10);
                break;
            case "November" :
                printMonth(year, 11);
                break;
            case "Desember" :
                printMonth(year, 12);
                break;
        }
    }

    static void printMonth(int year, int month) {

        // Print the headings of the calendar
        printMonthTitle(year, month);



        // Print the body of the calendar
        printMonthBody(year, month);

    }



    /** Print the month title, e.g., May, 1999 */

    static void printMonthTitle(int year, int month) {

        System.out.println("         " + getMonthName(month) + " " + year);

        System.out.println("-----------------------------");

        System.out.println(" Min Sen Sel Rab Kam Jum Sab");

    }



    /** Get the English name for the month */

    static String getMonthName(int month) {

        String monthName = null;

        switch (month) {

            case 1: monthName = "Januai"; break;

            case 2: monthName = "Februari"; break;

            case 3: monthName = "Maret"; break;

            case 4: monthName = "April"; break;

            case 5: monthName = "Mei"; break;

            case 6: monthName = "Juni"; break;

            case 7: monthName = "Juli"; break;

            case 8: monthName = "Augustus"; break;

            case 9: monthName = "September"; break;

            case 10: monthName = "Oktober"; break;

            case 11: monthName = "November"; break;

            case 12: monthName = "Desember";

        }

        return monthName;

    }



    /** Print month body */

    static void printMonthBody(int year, int month) {
        // Get start day of the week for the first date in the month
        int startDay = getStartDay(year, month);
        // Get number of days in the month
        int numberOfDaysInMonth = getNumberOfDaysInMonth(year, month);
        // Pad space before the first day of the month
        int i = 0;

        for (i = 0; i < startDay; i++)

            System.out.print("    ");



        for (i = 1; i <= numberOfDaysInMonth; i++) {

            if (i < 10)

                System.out.print("   " + i);

            else

                System.out.print("  " + i);



            if ((i + startDay) % 7 == 0)

                System.out.println();

        }

        System.out.println();

    }



    /** Get the start day of the first day in a month */

    static int getStartDay(int year, int month) {

        // Get total number of days since 1/1/1800
        int startDay1800 = 3;
        int totalNumberOfDays = getTotalNumberOfDays(year, month);
        // Return the start day
        return (totalNumberOfDays + startDay1800) % 7;

    }



    /** Get the total number of days since January 1, 1800 */

    static int getTotalNumberOfDays(int year, int month) {

        int total = 0;
        // Get the total days from 1800 to year - 1
        for (int i = 1800; i < year; i++)

            if (isLeapYear(i))

                total = total + 366;

            else

                total = total + 365;



        // Add days from Jan to the month prior to the calendar month
        for (int i = 1; i < month; i++)

            total = total + getNumberOfDaysInMonth(year, i);



        return total;

    }



    /** Get the number of days in a month */

    static int getNumberOfDaysInMonth(int year, int month) {

        if (month == 1 || month == 3 || month == 5 || month == 7 ||

                month == 8 || month == 10 || month == 12)

            return 31;



        if (month == 4 || month == 6 || month == 9 || month == 11)

            return 30;



        if (month == 2) return isLeapYear(year) ? 29 : 28;



        return 0; // If month is incorrect
    }



    /** Determine if it is a leap year */

    static boolean isLeapYear(int year) {

        return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);

    }
}
