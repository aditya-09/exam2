package org.example;

public class No1 {
    public static void all(){
        int Y = 2020;
        int awal = 3;
        int spasi = awal;

        String[] bulan = {  "", "Januari", "Februari", "Maret", "April", "Mei", "Juni",
                "July", "August", "September",
                "October", "November", "December" };

        int[] hari = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        for (int b = 1; b <= 12; b++){
            System.out.println("\t \t "+ bulan[b] + " " + Y);

            System.out.println("_____________________________________");
            System.out.println("   M    S    S    R    K    J    S");

            spasi = (hari[b-1] + spasi)%7;

            for (int i = 0; i < spasi; i++){
                System.out.print("     ");
            }
            for (int i = 1; i <= hari[b]; i++) {
                System.out.printf(" %3d ", i);
                if ( ( (i + spasi) % 7 == 0) || (i == hari[b]) ) System.out.println();
            }

            System.out.println();
        }
    }
}
